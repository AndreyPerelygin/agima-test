import json
import sys
import re
import html

class Render(object):
	"""docstring for Render"""
	def __init__(self, data="[]"):
		super(Render, self).__init__()
		try:
			self.data = json.loads(data)
		except Exception as e:
			print (e)
			sys.exit()

	def withoutTag(self):
		result = []
		for el in self.data:
			if "title" in el.keys():
				result.append("<h1>{}</h1>".format(el["title"]))
			if "body" in el.keys():
				result.append("<p>{}</p>".format(el["body"]))

		return "".join(result)

	def withTag(self):
		result = []
		for el in self.data:
			for key in el.keys():
				result.append("<{0}>{1}</{0}>".format(key, el[key]))

		return "".join(result)

	def withTagList(self):
		result = ["<ul>"]
		for el in self.data:
			result.append("<li>")
			for key in el.keys():
				result.append("<{0}>{1}</{0}>".format(key, el[key]))
			result.append("</li>")
		result.append("</ul>")

		return "".join(result)

	def withTagListDeep(self):
		def recursionRender(data):
			result = []
			if isinstance(data, list):
				result.append("<ul>")
				for el in data:
					result.append("<li>")
					for key in el.keys():
						if isinstance(el[key], list):
							result.append("<{}>".format(key))
							result += recursionRender(el[key])
							result.append("</{}>".format(key))
						else:
							result.append("<{0}>{1}</{0}>".format(key, el[key]))
					result.append("</li>")
				result.append("</ul>")
			else:
				for key in data.keys():
					if isinstance(data[key], list):
						result.append("<{}>".format(key))
						result += recursionRender(data[key])
						result.append("</{}>".format(key))
					else:
						result.append("<{0}>{1}</{0}>".format(key, data[key]))
			return result

		result = recursionRender(self.data)

		return "".join(result)

	def withTagListDeepAttrs(self):
		def renderTagArgs(text):
			class_list = []
			regex = re.search("\.([\w_-]*)", text)
			while regex:
				regex = regex.regs
				class_list.append(text[regex[1][0]:regex[1][1]])
				text = text.replace(
					text[regex[0][0]:regex[0][1]],
					''
					)
				regex = re.search("\.([\w_-]*)", text)

			id_list = []
			regex = re.search("\#([\w_-]*)", text)
			while regex:
				regex = regex.regs
				id_list.append(text[regex[1][0]:regex[1][1]])
				text = text.replace(
					text[regex[0][0]:regex[0][1]],
					''
					)
				regex = re.search("\#([\w_-]*)", text)

			text = text.strip(" ")
			text += ' id="{}"'.format(" ".join(id_list)) if len(id_list) else ""
			text += ' class="{}"'.format(" ".join(class_list)) if len(class_list) else ""
			return text.split(" ")[0], text

		def recursionRender(data):
			result = []
			if isinstance(data, list):
				result.append("<ul>")
				for el in data:
					result.append("<li>")
					for key in el.keys():
						tag = renderTagArgs(key)
						if isinstance(el[key], list):
							result.append("<{}>".format(tag[1]))
							result += recursionRender(el[key])
							result.append("</{}>".format(tag[0]))
						else:
							result.append("<{0}>{2}</{1}>".format(tag[1], tag[0], html.escape(el[key])))
					result.append("</li>")
				result.append("</ul>")
			else:
				for key in data.keys():
					tag = renderTagArgs(key)
					if isinstance(data[key], list):
						result.append("<{}>".format(tag[1]))
						result += recursionRender(data[key])
						result.append("</{}>".format(tag[0]))
					else:
						result.append("<{0}>{2}</{1}>".format(tag[1], tag[0], html.escape(data[key])))
			return result

		result = recursionRender(self.data)

		return "".join(result)


if __name__ == "__main__":
	if len(sys.argv) < 2:
		print ("""
Please, select render mode:
withoutTag - the first task
witTag - the second task
witTagList - the third task
withTagListDeep - the fourth task
withTagListDeepAttrs - the fifth task
			""")
		sys.exit()
	file_name = sys.argv[2] if len(sys.argv) > 2 else "source.json"
	with open(file_name, "r") as file:
		render = Render(data=file.read())
	try:
		print (render.__getattribute__(sys.argv[1])())
	except Exception as e:
		print (e)
